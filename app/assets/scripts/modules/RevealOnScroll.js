import $ from 'jquery';
import waypoints from '../../../../node_modules/waypoints/lib/noframework.waypoints';

class RevealOnScroll {
    constructor(els,offset){
        this.itemsToReveal = $(els);
        this.offsetPercentage = offset;
        this.hideIntially();
        this.createWaypoints();
    }
    
    hideIntially(){
        this.itemsToReveal.addClass("reveal-item");
    }
    
    createWaypoints(){
        var that = this;
        this.itemsToReveal.each(function(){
            var currentElement = this;
            new Waypoint({
                element : currentElement,
                handler : function(){
                $(currentElement).addClass("reveal-item__is-visible");
            },
                offset : that.offsetPercentage
            });
        });
    }
}

export default RevealOnScroll;